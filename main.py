"""
Converts an XForm-formatted ODK questionnaire into a Word document that meets PARC specifications.
Created for Forcier Consulting. (c) 2017 Langer Research Associates

LICENSE INFORMATION FOR THE XLS2JSON SCRIPT
Copyright (c) 2015, XLSForm
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""

from flask import Flask, request, render_template, redirect, url_for, send_from_directory
import pyxform
from pyxform.xls2json_backends import xls_to_dict
from docx import Document
import re
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)
doc = Document()
filenameforclean = ""

UPLOAD_FOLDER = "/home/langerresearch/forcierweb/converted"

app.config["MAX_CONTENT_LENGTH"] = 20 * 1024 * 1024
app.config["UPLOAD_FOLDER"] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = set(["xlsx", "xls", "docx"])

@app.route("/")
def hello():
    global doc
    doc = Document()
    return render_template("index.html")

def allowed_file(filename):
    return "." in filename and filename.rsplit(".",1)[1].lower() in ["xlsx","xls"]

@app.route("/reset")
def clean_me():
    folder = '/home/langerresearch/forcierweb/converted/'
    try:
        file_path = os.path.join(folder,filenameforclean)
        if os.path.isfile(file_path):
            print(file_path)
            os.unlink(file_path)
    except Exception as e:
            print(e)
    return redirect(url_for("hello"))

@app.route("/",methods=["POST"])
def upload_file():
    global doc
    global filenameforclean
    if request.method == "POST":
        # Make sure there is a file
        if "file_to_convert" not in request.files:
            return redirect(request.url)
        # Get the file
        xlsx_form = request.files["file_to_convert"]
        # Really make sure there's a file
        if xlsx_form.filename == "" or not allowed_file(xlsx_form.filename):
            return redirect(request.url)
        # Do everything now that you're satisfied
        if xlsx_form:
            # Pass through the pyxform XLS to JSON/dict converter
            xlsx_as_dict = pyxform.xls2json.workbook_to_json(xls_to_dict(xlsx_form))
            # Add the filename as a header
            xlsx_filename = secure_filename(xlsx_form.filename).split(sep=".")[0]
            doc.add_heading(xlsx_filename,0)
            # Print out as a docx
            surveylooper(xlsx_as_dict)
            filename = secure_filename(xlsx_filename+'.docx')
            filenameforclean = filename
            doc.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            return redirect(url_for('uploaded_file',filename=filename))
        return ""

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],filename)

def surveylooper(json_dict):
    global doc
    """Loops through a JSON-converted ODK survey"""
    for item in json_dict["children"]:
        if item["type"] == "group":
            # print(item["name"])
            surveylooper(item)
        else:
            result = questionprinter(item)
            if result: doc.add_paragraph(result)

def questionprinter(question_dict):
    """Prints a question with a faux number (if real not available), name, label, and instructs."""
    questionstring = ""
    mainlang = "English"
    # Print the question wording (label) first, accompanied by question number and question name. 
    if "label" in question_dict:
        qsearch = re.compile("(^Q?[0-9]+[a-z\.]*[0-9]?\.)\s([\s\S]+)")
        # Check to see if there are multiple labels or not
        if isinstance(question_dict["label"],dict):
            # If English is not there, use the first key available.
            if mainlang not in question_dict["label"]:
                mainlang = next(iter(question_dict["label"]))
            question_text = question_dict["label"][mainlang]
        else: question_text = question_dict["label"]

        # Add "fake question number" Q0. to numbers that don't have them

        if not qsearch.match(question_text): questionstring = questionstring+"Q0. "+"["+question_dict["name"]+"] "+question_text
        else: questionstring = questionstring+qsearch.match(question_text).group(1)+" ["+question_dict["name"]+"] "+qsearch.match(question_text).group(2)

    # If there is a relevant instruct, print that immediately after the question
    if "bind" in question_dict and "relevant" in question_dict["bind"]:
        questionstring = questionstring+" [[IF: "+question_dict["bind"]["relevant"]+"]]\n"
    elif questionstring:
        questionstring = questionstring+"\n"

    # Print answer choices
    if "choices" in question_dict:
        # Check to see if there are multiple choices
            for choice in question_dict["choices"]:
                if isinstance(choice["label"],dict): 
                    if mainlang not in choice["label"]:
                        mainlang = next(iter(choice["label"]))
                    questionstring = questionstring+choice["label"][mainlang]+"\n"
                else: questionstring = questionstring+choice["label"]+"\n"
    return questionstring

# if __name__ == '__main__':
#     # This is used when running locally.