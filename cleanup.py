# Adjusted cleanup script which just cleans stuff up that's more than a day old.
# Should be good for anything that doesn't get reset (assuming most things?)

import os, time
from os import path

current_time = time.time()

folder = '/home/langerresearch/forcierweb/converted/'

for file in os.listdir(folder):
    try:
        file_path = os.path.join(folder,file)
        creation_time = os.path.getmtime(file_path)
        if (current_time - creation_time) // (24 * 3600) >= 2:
            os.unlink(file_path)
    except Exception as e:
        print(e)
